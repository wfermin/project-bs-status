<?php
//DESARROLLADO POR WILFREDO FERMIN R.
//wilfredofermin@outlook.com | facebook/wilfredofemin
//about.me/wilfredofermin
//-------------------------------------------------------------
//INFORMACION GENERAL

/*CONEXION REMOTA CON SQL SERVER |
 * INSTALAR DRIVER PARA QUE FUNCIONES | PHP DRIVER :
 * https://www.microsoft.com/en-us/download/details.aspx?id=20098
 * INSTALAR ODBC DRIVER PARA SQL SERVER:
 * https://www.microsoft.com/en-us/download/details.aspx?id=36434
 * LAS ALERTAS UTILIZADAS SON SWEETALERT
 * http://t4t5.github.io/sweetalert/
 * Referencia : https://aprendible.com/blog/video-mensajes-de-sesion-con-sweet-alert-en-laravel-53
 *
 * VERVION DE PHP UTILIZADDO : 5.6
 * VERSION PHP FRAMEWORK LARAVEL 5.1
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function login()

    {

        return view('user.login');
    }

public function postLogin(Request $request)
{
    //VALIDACIONES
    $this->validate($request, [
        'email' => 'required|email|min:5|max:255',
        'password' => 'required|min:5|max:30'
    ]);

    //ENTRADAS
    $email = $request->input('email');
    $pw = $request->input('password');

    //BASE DE DATOS//////////////////////////////////////////////////////

    $users=DB::table('users')->where('email','like','%'.$email)->get();

    /*
      $results = DB::table('users')
          ->select(DB::raw("id,name,type,email,activo 'password'"))
          ->where('email', 'LIKE','%'. $email);

          $useramussol = DB::table('CbsAmosol.dbo.gymSocio')
              ->select(DB::raw("nombres,apellidos,idSocioStatus,RNC_Cedula,idsocio 'socio'"))
              ->where('idsocio', '=', $codigiosocio);

          //UNION DE LAS BASE DE DATOS //////////////////////////////////////////
          $results = $userbodyshop->union($useramussol)->get();
      */
    ///////////////////////////////////////////////////////////////////////

    //RECORRIENDO LOS DATOS

    if (!empty($users) ) {
    foreach ($users as $user) {
        $nombre = $user->name; //. " " . $user->apellidos;
        $pwData = $user->password;
        $status = $user->activo;
             if (($status) == 1) {

                if ($pwData == $pw) {

                    return view('auth.admin');
                } else {
                    alert()->error('Verifique su cuenta o password', 'Datos incorrectos ')->autoclose(3800);
                    return redirect('/login');

                }

            } else {
                alert()->error('Estatus de cuenta', 'Su cuenta no esta activa')->autoclose(3800);
                return redirect('/login');

            }

        }

    }else{
        alert()->warning('Su cuenta no esta registrada', 'Uops!')->autoclose(3800);
        return redirect('/login');
    }

    //ZONA DE PRUEBAS/////////////////////////////////////////////////////////////////////
    // return view('user.notify', compact('nombres', 'cedula','status')); CON ESTE METODO PASO LAS VARIALBLES A OTRA VISTA
    // $users = DB::select('select * from CbsAmosol.dbo.gymSocio where idsocio= ?', [$codigiosocio]);
    // $users=DB::table('CBSBodyShop.dbo.gymSocio')->where('idsocio','=',$codigiosocio)->get();
    // dd($results);

    }

    }
